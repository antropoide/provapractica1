package com.jpaMvn.ex17;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.jpaMvn.config.AppConfig;
import com.jpaMvn.entity.Person;
import com.jpaMvn.service.PersonService;

public class App 
{
    public static void main( String[] args )
    {
        AnnotationConfigApplicationContext context = 
                new AnnotationConfigApplicationContext(AppConfig.class);

          PersonService personService = context.getBean(PersonService.class);

          // Add Persons
          personService.addPerson(new Person("Sunil", "Bora", "suni.bora@example.com"));
          personService.addPerson(new Person("David", "Miller", "david.miller@example.com"));
          personService.addPerson(new Person("Sameer", "Singh", "sameer.singh@example.com"));
          personService.addPerson(new Person("Paul", "Smith", "paul.smith@example.com"));

          // Get Persons
          List<Person> persons = personService.listPersons();
          for (Person person : persons) {
             System.out.println("Id = "+person.getId());
             System.out.println("First Name = "+person.getFirstName());
             System.out.println("Last Name = "+person.getLastName());
             System.out.println("Email = "+person.getEmail());
             System.out.println();
          }
          context.close();
    }
}
