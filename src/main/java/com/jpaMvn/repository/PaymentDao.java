package com.jpaMvn.repository;

import java.util.List;

import com.jpaMvn.entity.Payment;

public interface PaymentDao {
	void addPayment(Payment payment);
	List<Payment> listPayments();

}
