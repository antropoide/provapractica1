package com.jpaMvn.repository;

import java.util.List;

import com.jpaMvn.entity.Person;

public interface PersonDao {
	
	//add register person
	void addPerson(Person person);
	//Read all persons
	List<Person> listPersons();

}
