package com.jpaMvn.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.jpaMvn.entity.Person;

@Repository
public class PersonDaoImp implements PersonDao{
    /*
   read: https://docs.oracle.com/javaee/6/tutorial/doc/gjrij.html
   The Criteria API operates on this abstract schema to allow developers to find,
    modify, and delete persistent entities by invoking Java Persistence API entity operations.
    */	

    //1.- Use an EntityManager instance to create a CriteriaBuilder object.	
	@PersistenceContext
	private EntityManager em;
		
	//methods
	//add register person

	public void addPerson(Person person) {
	em.persist(person);
	}
	
	//Read all persons
	//2. - Create a query object by creating an instance of the CriteriaQuery interface.
	//	This query object’s attributes will be modified with the details of the query.	
	   @Override
   public List<Person> listPersons() {
      CriteriaQuery<Person> criteriaQuery = em.getCriteriaBuilder().createQuery(Person.class);

      /*
     	3.- Set the query root by calling the from method on the CriteriaQuery object.
      	4.-Specify what the type of the query result will be by calling the select method
      	 of the CriteriaQuery object.
   		5.- Prepare the query for execution by creating a TypedQuery<T> instance, specifying
   		 the type of the query result.
   		6.-Execute the query by calling the getResultList method on the TypedQuery<T> object.
   		 Because this query returns a collection of entities, the result is stored in a List
       */ 
      @SuppressWarnings("unused")
      Root<Person> root = criteriaQuery.from(Person.class);
      return em.createQuery(criteriaQuery).getResultList();
   }

}
