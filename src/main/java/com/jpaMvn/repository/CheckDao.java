package com.jpaMvn.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Service;

import com.jpaMvn.entity.Check;
import com.jpaMvn.entity.Payment;

@Service
public class CheckDao implements PaymentDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void addPayment(Payment payment) {
		em.persist((Check) payment);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Payment> listPayments() {
		CriteriaQuery<Check> criteriaQuery = em.getCriteriaBuilder().createQuery(Check.class);
		return (List<Payment>)criteriaQuery;
	}
}
