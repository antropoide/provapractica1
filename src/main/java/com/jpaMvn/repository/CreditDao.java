package com.jpaMvn.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Repository;

import com.jpaMvn.entity.Credit;
import com.jpaMvn.entity.Payment;


@Repository
public class CreditDao implements PaymentDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void addPayment(Payment payment) {
		em.persist((Credit) payment);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Payment> listPayments() {
		CriteriaQuery<Credit> criteriaQuery = em.getCriteriaBuilder().createQuery(Credit.class);
		return (List<Payment>)criteriaQuery;
	}

}
