package com.jpaMvn.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Service;

import com.jpaMvn.entity.Cash;
import com.jpaMvn.entity.Credit;
import com.jpaMvn.entity.Payment;

@Service
public class CashDao implements PaymentDao  {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public void addPayment(Payment payment) {
		em.persist((Cash) payment);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Payment> listPayments() {
		CriteriaQuery<Cash> criteriaQuery = em.getCriteriaBuilder().createQuery(Cash.class);
		return (List<Payment>)criteriaQuery;
	}

}
