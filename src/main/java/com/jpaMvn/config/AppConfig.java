package com.jpaMvn.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
	/*
	The @configuration spring annotation can also be used to configure the Spring IOC container.
	 Here programmer add the @configuration to the Java class and this class is considered as special 
	 configuration class. The @configuration annotation method is targeted by the developers.
	 This is pure-java approach to configure the Spring IoC Container.
	 The @Bean  tag is used to define the beans, then Spring framework executes the method and then
	 register the object returned with an unique id. By default the name of the method is used
	 as the bean name.
	 Shortly, this class plus the persistence.xml configure the context (persistence) environment 
	*/

	@Configuration
	
	//Enables @Transaction annotations in Service classes, which guarantee the ACID commitment in
	//db transactions.
	   /*
	    * @Transaction:
	    * TransactionAttribute implementation that works out
	    * whether a given exception should cause transaction rollback
	    * by applying a number of rollback rules, both positive and negative.
	    * If no rules are relevant to the exception,
	    * it behaves like DefaultTransactionAttribute (rolling back on runtime exceptions).
	    */
	   
	@EnableTransactionManagement
	
	//Describes the packages paths where look for @Component annotations
	//and its derived stereotypes: @Entity, @Controller, @Service, @Repository.
	@ComponentScans(value = { @ComponentScan("com.jpaMvn.repository"),
	      @ComponentScan("com.jpaMvn.service"), @ComponentScan("com.jpaMvn.entity") })
	public class AppConfig {

	   //Autowire to service persistence (Called by MainApp.java)
	@Bean
	   public LocalEntityManagerFactoryBean geEntityManagerFactoryBean() {
	      LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
	      factoryBean.setPersistenceUnitName("LOCAL_PERSISTENCE");
	      return factoryBean;
	   }

	//service to repository persistence (Called by MainApp.java)
	   @Bean
	   public JpaTransactionManager geJpaTransactionManager() {
	      JpaTransactionManager transactionManager = new JpaTransactionManager();
	      transactionManager.setEntityManagerFactory(geEntityManagerFactoryBean().getObject());
	      return transactionManager;
}
	}
