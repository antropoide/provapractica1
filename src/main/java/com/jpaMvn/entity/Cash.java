package com.jpaMvn.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Cash
 *
 */
@Entity
@Table(name= "Cash")

public class Cash extends Payment implements Serializable {

	   
	@Id
	private int id;
	@Column(name= "Amount")
	private double amount;
	@Column(name= "Getted")
	private double getted;
	
	private static final long serialVersionUID = 1L;

	
		//constructor
	public Cash() {
		super();
	}  
	//METHODS
	@Override
	public String toString() {
		return "Cash [id=" + id + ", amount=" + amount + ", getted=" + getted + "]";
	}

		//getters and setters
	public int getId() {
		return this.id;
	}
 
	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}   
	public double getGetted() {
		return this.getted;
	}

	public void setGetted(double getted) {
		this.getted = getted;
	}

	
}
