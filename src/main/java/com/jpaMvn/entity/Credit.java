package com.jpaMvn.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Credit
 *
 */
@Entity
@Table(name= "Credit")

public class Credit extends Payment implements Serializable {

	
	private static final long serialVersionUID = 1L;

		//attributes
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(name= "User_name")
	private String userName;
	
	@Column(name = "Card_num")
	private String cardNum;
	
	@Column(name= "Amount")
	private double amount;
	
	@Column(name= "Available")
	private double available;
   
		//constructors
	public Credit() {
		super();
	}

		//methods
	@Override
	public String toString() {
		return "Credit [id=" + id + ", userName=" + userName + ", cardNum=" + cardNum + ", amount=" + amount
				+ ", available=" + available + "]";
	}
	
		//getters and setters
	public Long getId() {
		return id;
	}	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getCardNum() {
		return cardNum;
	}


	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public double getAvailable() {
		return available;
	}


	public void setAvailable(double amount) {
		this.available -= amount;
	}

	
	
}
