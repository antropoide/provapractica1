package com.jpaMvn.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Check
 *
 */
@Entity
@Table(name= "Check")

public class Check extends Payment implements Serializable {
	
	//attributes
	
	@Id
	private int id;
	@Column(name= "User_name")
	private String userName;
	@Column(name= "Amount")
	private double amount;
	
	private static final long serialVersionUID = 1L;
	
	//constructor

	public Check() {
		super();
	}   

	//METHODS
	@Override
	public String toString() {
		return "Check [id=" + id + ", userName=" + userName + ", amount=" + amount + "]";
	}


		//getters & setters
	public int getId() {
		return this.id;
	}


	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}   
	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

   
}
