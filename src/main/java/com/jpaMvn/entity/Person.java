package com.jpaMvn.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Person
 * POJO 
 */
@Entity
@Table(name= "Persons")

public class Person implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	//fields
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(name= "First_name")
	private String firstName;
	
	@Column(name="Last_name")
	private String lastName;
	
	@Column(name = "E_mail")
	private String email;
	
	//constructor
	public Person() {
		super();
	}
	
	 public Person(String firstName, String lastName, String email) {
	      this.firstName = firstName;
	      this.lastName = lastName;
	      this.email = email;
	 }

	 //methods
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	 

	 
	
	
}
