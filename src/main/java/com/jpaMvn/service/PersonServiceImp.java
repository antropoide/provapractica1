package com.jpaMvn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jpaMvn.entity.Person;
import com.jpaMvn.repository.PersonDao;

@Service
public class PersonServiceImp implements PersonService{
	
	   @Autowired
	   private PersonDao userDao;
	   
	   /*
	    * @Transaction:
	    * TransactionAttribute implementation that works out
	    * whether a given exception should cause transaction rollback
	    * by applying a number of rollback rules, both positive and negative.
	    * If no rules are relevant to the exception,
	    * it behaves like DefaultTransactionAttribute (rolling back on runtime exceptions).
	    */
	   
	   
	   @Transactional
	   @Override
	   public void addPerson(Person person) {
	      userDao.addPerson(person);
	   }
	   
	   @Transactional(readOnly = true)
	   @Override
	   public List<Person> listPersons() {
	      return userDao.listPersons();
	   }

}
