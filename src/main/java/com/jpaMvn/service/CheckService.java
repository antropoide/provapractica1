package com.jpaMvn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpaMvn.entity.Payment;
import com.jpaMvn.repository.CheckDao;

@Service
public class CheckService implements PaymentService{
	
	//wired Attributes
	@Autowired
	public CheckDao checkDao;
	
	//methods from interface
	public void addPayment(Payment payment) {
		checkDao.addPayment(payment);
	}
	public List<Payment> listPayments(){
		return checkDao.listPayments();
	}
	
	//own methods

		
}
