package com.jpaMvn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpaMvn.entity.Cash;
import com.jpaMvn.entity.Payment;
import com.jpaMvn.repository.CashDao;

@Service
public class CashService implements PaymentService{
	
	//wired Attributes
	@Autowired
	private CashDao cashDao;
	
	//methods from interface
	
	public void addPayment(Payment payment) {
		cashDao.addPayment(payment);
	}
	public List<Payment> listPayments(){
		return cashDao.listPayments();
	}
	
	//own methods
	
	//make payment
	
	public void makePayment(Payment payment) {
		this.cashReturnNeeded((Cash)payment);
		this.addPayment(payment);		
	}
	
	//check if cash return is needed
	public void cashReturnNeeded(Cash cash) {
		if(cash.getGetted() > cash.getAmount()) {
			System.out.println("Change amount to return: " + (cash.getGetted()-cash.getAmount()));
		}
	}

}
