package com.jpaMvn.service;

import java.util.List;

import com.jpaMvn.entity.Payment;

public interface PaymentService {
	void addPayment(Payment payment);
	List<Payment> listPayments();

}
