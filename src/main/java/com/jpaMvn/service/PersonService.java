package com.jpaMvn.service;

import java.util.List;

import com.jpaMvn.entity.Person;

public interface PersonService {
		//add register person
		void addPerson(Person person);
		//Read all persons
		List<Person> listPersons();
}
