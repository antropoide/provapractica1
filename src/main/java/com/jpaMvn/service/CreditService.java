package com.jpaMvn.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpaMvn.entity.Credit;
import com.jpaMvn.entity.Payment;
import com.jpaMvn.repository.CreditDao;

@Service
public class CreditService implements PaymentService{
	
	//wired attribute
	@Autowired
	private CreditDao creditDao;
	
	//methods from interface
	
	@Transactional
	public void addPayment(Payment payment) {
		creditDao.addPayment(payment);
	}
	
	 @Transactional
	 public List<Payment> listPayments(){
		 return creditDao.listPayments();
					
	 }
	 
	 //Credit own methods
	 //Make Credit
	 public boolean makeCredit(Payment payment) {
		 if(this.enoughBalance((Credit) payment)) {
			 this.applyAmount((Credit) payment);
			 this.addPayment((Credit) payment);
			 return true;
		 }
		 return false;
	 }
	 	//check enough balance
	 public boolean enoughBalance(Credit credit) {
		 if(credit.getAvailable() >= credit.getAmount()) {			 
			 return true;
		 }
		 return false;
		 
	 }
	 	//recalculate credit card available
	 public void applyAmount(Credit credit) {
		 credit.setAvailable(credit.getAvailable()- credit.getAmount());
	 }



}
